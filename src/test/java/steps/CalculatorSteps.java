package steps;

import com.ing.Calculator;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

/**
 * Created by Marius Dima on 22.07.2019.
 */
public class CalculatorSteps {

    private Calculator calculator;

    @Given("^I initialize the calculator$")
    public void iInitializeTheCalculator() {
        this.calculator = new Calculator();
    }


    @When("^I perform an addition of (\\d+) and (\\d+)$")
    public void iPerformAnAdditionOfAnd(int arg0, int arg1) {
        calculator.add(arg0, arg1);
    }

    @Then("^Displayed result is ([^\"]*)$")
    public void displayedResultIs(int expectedResult) {
        long actualResult = calculator.getDisplayedResult();
        Assert.assertEquals(expectedResult, actualResult);
    }

    @When("^I perform a multiplication of ([^\"]*) and ([^\"]*)$")
    public void iPerformAMultiplicationOfOperandAndOperand(int operand1, int operand2) {
        calculator.multiply(operand1, operand2);
    }
}
