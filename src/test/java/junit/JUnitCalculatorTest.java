package junit;

import com.ing.Calculator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Marius Dima on 22.07.2019.
 */
public class JUnitCalculatorTest {

    @Test
    public void testCalculatorShouldAddInMemory() {
        Calculator calculator = new Calculator();
        calculator.add(1, 2);
        Assert.assertEquals("Displayed result is not correct!", 3, calculator.getDisplayedResult());
    }
}
