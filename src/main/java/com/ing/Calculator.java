package com.ing;

/**
 * Created by Marius Dima on 22.07.2019.
 */
public class Calculator {

    private long displayedResult;

    public long getDisplayedResult() {
        return displayedResult;
    }

    public void setDisplayedResult(long displayedResult) {
        this.displayedResult = displayedResult;
    }

    public void add(int a, int b) {
        this.displayedResult = a + b;
    }

    public void subtract(int a, int b) {
        this.displayedResult = a - b;
    }

    public void multiply(int a, int b) {
        this.displayedResult = a * b;
    }

    public void divide(int a, int b) {
        if (b == 0) throw new IllegalArgumentException("Divider cannot be 0!");
        this.displayedResult = a / b;
    }

}
