Feature: Math Operations

  Background:
    Given I initialize the calculator


  Scenario: Addition
    When I perform an addition of 2 and 5
    Then Displayed result is 7


  Scenario Outline:
#    Given I initialize the calculator
    When I perform a multiplication of <operand1> and <operand2>
    Then Displayed result is <result>
    Examples:
      | operand1 | operand2 | result |
      | 1        | 3        | 3      |
      | 2        | 3        | 6      |
